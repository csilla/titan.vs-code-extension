/******************************************************************************
 * Copyright (c) 2000-2022 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/

/**
 * @author Miklos Magyari
 */
import fs = require('fs');
import { XMLParser } from 'fast-xml-parser';
import { workspace } from 'vscode';

/* tpd root element */
const tpdRoot = 'TITAN_Project_File_Information';

/* first level elements */
const tpdProjectName = 'ProjectName';
const tpdReferencedProjects = 'ReferencedProjects';
const tpdFolders = 'Folders';
const tpdFiles = 'Files';
const tpdFileResource = 'FileResource';
const tpdFolderResource = 'FolderResource';

/* 
    Members should keep the underscore prefix as in the parser we
    use this character as the prefix for xml tag attributes
*/
interface FileResource {
    _projectRelativePath?: string,
    _relativeURI?: string,
    _rawURI?: string
}

interface FolderResource {
    _projectRelativePath?: string,
    _relativeURI?: string,
    _rawURI?: string
}

export class Tpd {
    isTpd: boolean = false;
    projectName!: string;
    folders!: FolderResource[];
    files!: FileResource[];

    getProjectName() : string {
        return this.projectName;
    }

    isValidTpd() : boolean {
        return this.isTpd;
    }

    parseXml(xmldata: string) {
        let options = {
            ignoreAttributes: false,
            attributeNamePrefix: '_'
        };
        const parser = new XMLParser(options);
        
        const xmlObject = parser.parse(xmldata);
        console.log(xmlObject);    

        if (xmlObject[tpdRoot] !== undefined) {
            this.isTpd = true;
            this.projectName = xmlObject[tpdRoot][tpdProjectName];
            this.folders = xmlObject[tpdRoot][tpdFolders][tpdFolderResource];
            this.files = xmlObject[tpdRoot][tpdFiles][tpdFileResource];

            this.files.forEach((x) => {
                console.log(x._projectRelativePath);
            });
        } else {
            this.isTpd = false;
            console.log("This file does not seem to be a valid TPD file.");
        }
    }
}

export function findTpdFiles() : string[] {
    if (workspace.workspaceFolders !== undefined) {
        let folder = workspace.workspaceFolders[0].uri.fsPath;
        console.log("Project:", folder);
    }
    let retval!: string[];
    return retval;
}
