/******************************************************************************
 * Copyright (c) 2000-2022 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/

/**
 * TreeDataProvider for the TPD files view
 * 
 * @author Miklos Magyari
 */
import { ConfigurationTarget, Event, EventEmitter, ProviderResult, TreeDataProvider, TreeItem, TreeItemCollapsibleState, workspace } from "vscode";
import path = require('path');

export class TpdTreeProvider implements TreeDataProvider<TpdFile> {
    tpdFiles: string[];

    constructor(private workspaceRoot: string|undefined) {
        this.tpdFiles = [];
    }

    getTreeItem(element: TpdFile) : TreeItem {
        return element;
    }

    getChildren(element?: TpdFile | undefined): ProviderResult<TpdFile[]> {
        let items: TpdFile[] = [];
        this.tpdFiles.forEach(tpd => items.push(new TpdFile(tpd, TreeItemCollapsibleState.None)));
        return Promise.resolve(items);
    }

    addItem(newitem: string) {
        const name = formatTpdName(newitem);
        if (!this.tpdFiles.includes(name)) {
            this.tpdFiles.push(name);
            this.refresh();
            
            const config = workspace.getConfiguration('titan');
            config.update('compiler.tpdFiles', this.tpdFiles, ConfigurationTarget.Workspace);
        }
    }

    loadItems() {
        const config = workspace.getConfiguration('titan');
        let loaded = config.get('compiler.tpdFiles');
        if (loaded !== undefined) {
            this.tpdFiles = [];
            (loaded as string[]).forEach(tpdfile => {
                this.tpdFiles.push(formatTpdName(tpdfile));
            });
            this.refresh();
        }
    }

    private _onDidChangeTreeData: EventEmitter<TpdFile | undefined | null | void> = new EventEmitter<TpdFile | undefined | null | void>();
    readonly onDidChangeTreeData?: Event<void | TpdFile | TpdFile[] | null | undefined> = this._onDidChangeTreeData.event;

    refresh() : void {
        this._onDidChangeTreeData.fire();
    }
}

// Format TPD filename as 'filename - folder'
function formatTpdName(filename: string) : string {
    const parsed = path.parse(filename);
    return parsed.base + " - " + parsed.dir;
}

class TpdFile extends TreeItem {
    constructor(
        public readonly label: string,
        public readonly collapsible: TreeItemCollapsibleState
    ) {
        super(label, collapsible);
    }
}