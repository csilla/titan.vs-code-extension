/******************************************************************************
 * Copyright (c) 2000-2022 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/

/**
 * @author Miklos Magyari
 * @author Adam Knapp
 */
'use strict';

import * as fs from "fs";
import * as path from 'path';
import * as net from 'net';
import * as child_process from "child_process";
import { commands, Disposable, ExtensionContext, OpenDialogOptions, Progress, ProgressLocation, TextDocument, TextDocumentChangeEvent, Uri, window, workspace } from 'vscode';
import { 
	DidChangeTextDocumentNotification, DidChangeTextDocumentParams, DidOpenTextDocumentNotification, DidOpenTextDocumentParams, LanguageClient, LanguageClientOptions, 
	NotificationType, StreamInfo, WorkDoneProgressReport 
} from 'vscode-languageclient/node';
import { findTpdFiles } from './tpd';
import { TpdTreeProvider } from "./tpdprovider";

let client: LanguageClient;
let progressListener: ProgressListener|null = null;

class ProgressListener {
	progress!: Progress<{ message: string; increment?: number; }> | null;
	resolve!: ((nothing: {}) => void);

	startProgress(message: string) {
		if (this.progress !== null && this.progress !== undefined) {
			this.endProgress();
		}
		window.withProgress({
			location: ProgressLocation.Window,
			cancellable: false,
			title: message
		}, progress => new Promise((resolve, _reject) => {
			this.progress = progress;
			this.resolve = resolve;
		}));
	}
	
	reportProgress(message: string, increment: number) {
		this.progress?.report({message, increment});
	}

	endProgress() { 
		if (this.progress !== null) {
			this.resolve({});
			this.progress = null;
			this.resolve = (({}) => {});
		}
	}
};

export function activate(context: ExtensionContext) {
	function createServer(): Promise<StreamInfo> {
			return new Promise((resolve, _reject) => {
				var server = net.createServer((socket) => {
					console.log("Creating server");
				
				resolve({
					reader: socket,
					writer: socket
				});

				socket.on('end', () => console.log("Disconnected"));
			}).on('error', (err) => {
				// handle errors here
				throw err;
			});

			let javaExecutablePath = findJavaExecutable('java');

			server.listen(5555, () => {
				let options = { cwd: workspace.rootPath };
				let address = server.address();
				let info: net.AddressInfo = address as net.AddressInfo;
				console.log(info.port.toString());
				let args = [
					'-jar',
					path.resolve(context.extensionPath, 'server', 'org.eclipse.titan.lsp.jar'),
					info.port.toString()
				];

				// let argu: string[] = args as string[];

				// if (javaExecutablePath !== null) {
				// 	let process = child_process.spawn(javaExecutablePath, argu, options);
					
				// 	let logdir = "c:\\download\\00log";

				// 	// Send raw output to a file
				// 	if (!fs.existsSync(logdir)) {
				// 		fs.mkdirSync(logdir);
				// 	}
					
				// 	let logFile = logdir + '/titan-languageserver-java.log';
				// 	let logStream = fs.createWriteStream(logFile, { flags: 'w' });
					
				// 	process.stdout.pipe(logStream);
				// 	process.stderr.pipe(logStream);
					
				// 	console.log(`Storing log in '${logFile}'`);
				// }
			});
		});
	};

	// Options to control the language client
	let clientOptions: LanguageClientOptions = {
		// Register the server for plain text documents
		documentSelector: [{ scheme: 'file', language: 'ttcn'}],
		
		synchronize: {
			// Synchronize the setting section 'languageServerExample' to the server
			configurationSection: 'titan',
			// Notify the server about file changes to '.clientrc files contain in the workspace
			fileEvents: workspace.createFileSystemWatcher('**/*'),
		}
	};

	let serverName = "ttcn3server";
	client = new LanguageClient(
		'ttcnserver', 
		serverName, 
		createServer, 
		clientOptions);
		
	let disposable = client.start();
	context.subscriptions.push(disposable);
	
	/** 
	 * A new file is created in the editor.
	 * If it has a file type of ttcn3, asn1 or config, we should notify the server.
	 * 
	 * TODO: only ttcn is handled right now
	 **/
	context.subscriptions.push(workspace.onDidOpenTextDocument((document: TextDocument) => {
		if (document.isUntitled && document.languageId === 'ttcn') {
			let params: DidOpenTextDocumentParams = {
				textDocument: {
					languageId: 'ttcn',
					text: '',
					uri: '^/file:///' + document.fileName,
					version: 0
				}
			};
			client.sendNotification(DidOpenTextDocumentNotification.type, params);
		}
		return;
	}));

	/** 
	 * A change is made to a file.
	 * If it has a file type of ttcn3, asn1 or config and it is still unsaved, we should notify the server.
	 * 
	 * TODO: only ttcn is handled right now
	 **/
	context.subscriptions.push(workspace.onDidChangeTextDocument((event: TextDocumentChangeEvent) => {
		if (event.document.isUntitled && event.document.languageId === 'ttcn') {
			let contentChanges = event.contentChanges.map((change) => ({
					range: { start: change.range.start, end: change.range.end },
					text: change.text
				}));

			let params: DidChangeTextDocumentParams = {
				textDocument: { uri: 'file:///' + event.document.fileName, version: event.document.version },
				contentChanges: contentChanges
			};
			client.sendNotification(DidChangeTextDocumentNotification.type, params);
		}
	}));

	const rootPath = workspace.workspaceFolders && workspace.workspaceFolders.length > 0 ? workspace.workspaceFolders[0].uri.fsPath : undefined;
	const tpdprovider = new TpdTreeProvider(rootPath);
	window.registerTreeDataProvider('tpdview', tpdprovider);
	window.createTreeView('tpdview', {
		treeDataProvider: tpdprovider
	});
	commands.registerCommand('tpdview.addFile', () => {
		let options: OpenDialogOptions = {
			filters: { tpd : [ 'tpd'] }
		};
		window.showOpenDialog(options).then(fileinfo => {
			if (fileinfo !== undefined) {
				let selected: Uri[] = fileinfo;
				selected.forEach(tpdfile => {
					tpdprovider.addItem(tpdfile.fsPath);
				});
			}
		});
	});
	tpdprovider.loadItems();

	client.onReady().then(() => {
		findTpdFiles();
		client.onNotification(new NotificationType('$/progress'), (e: WorkDoneProgressReport|any) => {
			if (progressListener === undefined || progressListener === null)	{
				progressListener = new ProgressListener();
				progressListener.startProgress(e.message);
			}
			if (e.value.kind === 'end') {
				progressListener.endProgress();
			} else {
				progressListener.reportProgress(e.value.message, e.value.percentage);
			}
		});
		
	});

	console.log('"titan" is now active! ');
}

export function deactivate(): Thenable<void> | undefined {
	if (!client) {
		return undefined;
	}
	client.stop();
}

// MIT Licensed code from: https://github.com/georgewfraser/vscode-javac
function findJavaExecutable(binname: string) {
	binname = correctBinname(binname);

	// First search each JAVA_HOME bin folder
	if (process.env['JAVA_HOME']) {
		let workspaces = process.env['JAVA_HOME'].split(path.delimiter);
		for (let i = 0; i < workspaces.length; i++) {
			let binpath = path.join(workspaces[i], 'bin', binname);
			if (fs.existsSync(binpath)) {
				return binpath;
			}
		}
	}

	// Then search PATH parts
	if (process.env['PATH']) {
		let pathparts = process.env['PATH'].split(path.delimiter);
		for (let i = 0; i < pathparts.length; i++) {
			let binpath = path.join(pathparts[i], binname);
			if (fs.existsSync(binpath)) {
				return binpath;
			}
		}
	}

	// Else return the binary name directly (this will likely always fail downstream) 
	return null;
}

function correctBinname(binname: string) {
	if (process.platform === 'win32') {
		return binname + '.exe';
	} else {
		return binname;
	}
}
